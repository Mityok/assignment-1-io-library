section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    	mov rax, 60	
	syscall
 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
  .loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
  .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    xor rax, rax
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rsi, rsp
    pop rdi
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n'
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov rdi, 10


	mov r8, rsp
	dec rsp
	mov byte[rsp], 0
.loop:
	xor rdx, rdx
	div rdi
	
	add rdx, '0'
	dec rsp
	mov byte[rsp], dl

	cmp rax, 0
	jz .exit
	jmp .loop


.exit:
	mov rdi, rsp
	push r8
	call print_string
	pop r8

	mov rsp, r8
	ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    mov rax, rdi
    push rax
    sar rax, 60
    and rax, 0xf   
    cmp rax, 7   
    jg .minus
		; if the first digit is 1, then the number is negative
		; 7 - 0111,   8 - 1000
    pop rax
    mov rdi, rax
    call print_uint
 ret


  .minus:
    mov rdi, '-'
    call print_char
    pop rax
    neg rax 
        mov rdi, rax
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r10
    xor rax, rax
    xor rcx, rcx
    xor r10, r10
   .loop:
    mov al, byte[rdi+r10]
    mov cl, byte[rsi+r10]
    cmp al, cl
    jnz .bad
  .good:
    inc r10
    cmp al, 0
    jnz .loop

.goodexit:
   mov rax, 1
   pop r10
   ret

  .bad:
    mov rax, 0
    pop r10
    ret





; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:

    xor rax, rax
    dec rsp       ; we don't use push
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp	
    syscall

    cmp rax, 0
    jz .exit	

    mov al, byte[rsp]  ;use 1 byte, not 8
    inc rsp   
    ret 

.exit:   ; if end of file
    inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rax, rax
	xor rdx, rdx
	xor r8, r8

.loop:
	push rsi
	push rdi
	push rdx
	push r8
	call read_char
	pop r8
	pop rdx
	pop rdi
	pop rsi
	cmp rax, 0xA
	je .loop
	cmp rax, 0x20
	je .loop
	cmp rax, 0x9
	je .loop
.nice_char:
	cmp rax, 0x0
	je .exit
	cmp rax, 0x20
	je .exit
	cmp rax, 0xA
	je .exit
	cmp rax, 0x9
	je .exit
	cmp r8, rsi
	jg .badexit

	mov byte [rdi+r8], al
	inc r8
	push rsi
        push rdi
        push rdx
	push r8
	call read_char
	pop r8
	pop rdx
	pop rdi
	pop rsi

	jmp .nice_char

.exit:
	
	mov byte [rdi+r8], 0x0
	mov rax, rdi
	mov rdx, r8
	
    ret

.badexit:
	mov rax, 0
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:

	push rdi
	push rbx

	xor rax, rax
	xor rdx, rdx
	xor rbx, rbx

	xor r8, r8
	xor r9, r9
	xor r10, r10
	push rsi
	mov rsi, 10

.loop:
	xor rcx, rcx
	mov cl, byte[rdi+r9]
	cmp cl, 0
	jz .exit
	cmp cl, "0"
	jz .null
	cmp cl, "9"
	jg .exit
	jmp .def
.def:
	mov r10, 0
	inc r9
	inc r8
	xor rcx, "0"
	mul rsi
	add rax, rcx
	jmp .loop
	

.null:
	inc r9
	cmp r8, 0
	jnz .nice_digit
	mov r10, 1
	jmp .loop

.nice_digit:
	mov r10, 0
	inc r8
	xor rcx, "0"
	mul rsi
	add rax, rcx
	jmp .loop

	
.exit:
	add r8, r10
	mov rdx, r8
	

	pop rsi


	pop rbx
	pop rdi

	ret







; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], '-'
	je .minus	
	call parse_uint
	ret
.minus:
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	xor r9, r9
	
.loop:
	xor rcx, rcx
	cmp rdx, 0
	je .exit
		dec rdx
	mov cl, [rdi+r9]
	mov byte [rsi+r9], cl
	inc r9

	cmp rcx, 0
	je .exit
	jmp .loop

.exit:
	ret
